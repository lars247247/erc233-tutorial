ERC233 token
===============

This is the tutorial for ERC233 token

## VM Environment
- Vagrant

## Project Version Information
- truffle v4.1.15
- ganache-cli v6.4.4
- solidity v0.4.25
- openzeppelin v1.9.0

## Install
- nodejs
```
        curl --silent --location https://rpm.nodesource.com/setup_9.x | sudo bash -
        yum -y install nodejs
```
- gcc+
```
        yum install gcc-c++ make
```
- truffle
```
        npm install -g truffle@v4.1.5
```
- node_modules
```
        npm install        
```
- ganache
```
        npm install ganache-cli -g
```

## How To Run Project
1. run private block chain in your dev, [please refer to run private blockchain anchor](#run-private-blockchain)
1. run test, [please refer to run truffle test anchor](#run-truffle-test)

## Run Private Blockchain
1. use ganache-cli
```
        ganache-cli -m "candy maple cake sugar pudding cream honey rich smooth crumble sweet treat" -h 0.0.0.0 
```
        
## Run Truffle Test
```
        truffle test
```
   