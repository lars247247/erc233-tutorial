pragma solidity ^0.4.18;

import './ERC223/ERC223Token.sol';
import './ERC223/ERC223ContractInterface.sol';
import 'openzeppelin-solidity/contracts/math/SafeMath.sol';

contract TutorialToken is ERC223Token{
	using SafeMath for uint256;
	
	string public constant name = 'TutorialToken';
	string public constant symbol = 'TT';
	uint8 public constant decimals = 18;
	uint256 public constant INITIAL_SUPPLY = 10000 * (10 ** uint256(decimals)); //create 10000 TT token

	/**
	* @dev Constructor
	*/
	constructor() public {
		mint(msg.sender, INITIAL_SUPPLY); //mint 10000 to smart contract creator
		finishMinting(); //stop mint
	}

	/**
	* @dev transfer token for a specified address
	* @param _to The address to transfer to.
	* @param _value The amount to be transferred.
	*/
	function transfer(address _to, uint256 _value) public returns (bool) {
		require(_to != address(0));
		require(_value <= balances[msg.sender]);
		require(_value > 0);

		bytes memory empty;

		// SafeMath.sub will throw if there is not enough balance.
		balances[msg.sender] = balances[msg.sender].sub(_value);
		balances[_to] = balances[_to].add(_value);

	    bool isUserAddress = false;
	    // solium-disable-next-line security/no-inline-assembly
	    assembly {
	      isUserAddress := iszero(extcodesize(_to))
	    }

	    //if this is the smart contract address
	    if (isUserAddress == false) {
	      ERC223ContractInterface receiver = ERC223ContractInterface(_to);
	      receiver.tokenFallback(msg.sender, _value, empty);
	    }

		emit Transfer(msg.sender, _to, _value);
		return true;
	}
}

