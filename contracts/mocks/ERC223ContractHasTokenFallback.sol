pragma solidity ^0.4.21;

import 'openzeppelin-solidity/contracts/math/SafeMath.sol';
import "openzeppelin-solidity/contracts/ownership/CanReclaimToken.sol";

/**
 * @title Contracts that can calcualate the total amount of tokens
 * @author Remco Bloemen <remco@2π.com>
 * @dev This blocks incoming ERC223 tokens to prevent accidental loss of tokens.
 * Should tokens (any ERC20Basic compatible) end up in the contract, it allows the
 * owner to reclaim the tokens.
 */
contract ERC223ContractHasTokenFallback is CanReclaimToken {
  using SafeMath for uint256;

  uint256 public total_amount = 0;

 /**
  * @dev accept all ERC223 compatible tokens
  * @param from_ address The address that is transferring the tokens
  * @param value_ uint256 the amount of the specified token
  * @param data_ Bytes The data passed from the caller.
  */
  function tokenFallback(address from_, uint256 value_, bytes data_) external {
   total_amount = total_amount.add(value_);
  }

 /**
  * @dev count total amount of token
  */
  function getTotal() public view returns(uint256){
    return total_amount;
  }
}
