require('babel-register');
require('babel-polyfill');

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: { // this will deploy to the dev testrpc or ganache-cli network
      host: "localhost",
      port: 8545,
      network_id: "*" // Match any network id
    }  
  }
};
