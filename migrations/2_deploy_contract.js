var TutorialToken = artifacts.require("./TutorialToken.sol");

module.exports = function(deployer, network, accounts){

    deployer.deploy(TutorialToken)
    .then(() => TutorialToken.deployed())
	.catch(e => {
        console.log(e);
    });
};
