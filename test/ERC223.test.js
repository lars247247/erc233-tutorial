import token from './helpers/token';
import { advanceBlock } from './helpers/advanceToBlock';
import EVMRevert from './helpers/EVMRevert';

const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

const ERC223ContractHasNoTokenFallback = artifacts.require("./mocks/ERC223ContractHasNoTokenFallback.sol");
const ERC223ContractHasRevert = artifacts.require("./mocks/ERC223ContractHasRevert.sol");
const ERC223ContractHasTokenFallback = artifacts.require("./mocks/ERC223ContractHasTokenFallback.sol");
const TutorialToken = artifacts.require("./TutorialToken.sol");

contract('ERC223', function ([token_holder, token_receiver, ERC223_contract_token_receiever, ERC223Mock_creator]) {

  before(async function () {
    // Advance to the next block to correctly read time in the solidity "now" function interpreted by testrpc
    await advanceBlock();
  });

  beforeEach(async function () {
    //create contract
    this.TutorialToken = await TutorialToken.new({from:token_holder});

    //mock contract create
    this.ERC223ContractHasRevert = await ERC223ContractHasRevert.new({from:ERC223Mock_creator});
    this.ERC223ContractHasTokenFallback = await ERC223ContractHasTokenFallback.new({from:ERC223Mock_creator});
    this.ERC223ContractHasNoTokenFallback = await ERC223ContractHasNoTokenFallback.new({from:ERC223Mock_creator});

  });

  describe('transaction check', function () {
    it('can accept transaction if address is not contract address', async function () {
      var tokens = web3.toWei(1000, "ether");

      //transfer 1000 token from token_holder to token_receiver
      await this.TutorialToken.transfer(token_receiver, tokens,{from: token_holder}).should.be.fulfilled;

      //check the token balance of token_receiver
      var  token_balance = await this.TutorialToken.balanceOf(token_receiver);
      token_balance = token_balance.toNumber(10);
      token_balance = token(token_balance);
      token_balance = parseInt(token_balance);

      //token_receiver should have 1000 token
      token_balance.should.equal(1000);

    });

    it('should reject if contract does not have tokenFallback', async function () {
      var tokens = web3.toWei(1000, "ether");

      //this transaction should be fail because this smart contract doesn't have token fall back function
      await this.TutorialToken.transfer(this.ERC223ContractHasNoTokenFallback.address, tokens,{from: token_holder}).should.be.rejectedWith(EVMRevert);

    });

    it('should reject if contract have revert in the tokenFallback', async function () {
      var tokens = web3.toWei(1000, "ether");

      //this transaction should be fail because this smart contract doesn't accept token
      await this.TutorialToken.transfer(this.ERC223ContractHasRevert.address, tokens,{from: token_holder}).should.be.rejectedWith(EVMRevert);

    });

    it('should accept if contract have no revert in the tokenFallback', async function () {
      //check the total amount of tokens
      var total_amount = await this.ERC223ContractHasTokenFallback.getTotal();
      total_amount = total_amount.toNumber(10);
      total_amount = parseInt(total_amount);

      //total amount should have be 0 at first
      total_amount.should.equal(0);

      var tokens = web3.toWei(1000, "ether");

      //this transaction should be success because this smart contract can accept token
      await this.TutorialToken.transfer(this.ERC223ContractHasTokenFallback.address, tokens,{from: token_holder}).should.be.fulfilled;

      total_amount = await this.ERC223ContractHasTokenFallback.getTotal();
      total_amount = web3.fromWei(total_amount, "ether");
      total_amount = total_amount.toNumber(10);
      total_amount = parseInt(total_amount);
      total_amount.should.equal(1000);
    });

  });

});
