var Migrations = artifacts.require("./Migrations.sol");

module.exports = function(deployer) {
  deployer.deploy(Migrations)
  .then(() => Migrations.deployed())
    .then(migrations => new Promise(resolve => setTimeout(() => resolve(migrations), 1000)))
    .then(migrations => console.log('ok'))
    .catch(e => {
        console.log(e);
    });
};
